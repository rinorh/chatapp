package ChatApp.server.tcp;

import ChatApp.httpUtils.HttpRequestProcessor;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class InnerHttpConnection implements Runnable {

    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private InnerHttpRequestHandler requestHandler;

    public InnerHttpConnection(Socket socket) {
        this.socket = socket;
        this.requestHandler = new InnerHttpRequestHandler(socket);
        setupConnection();
    }

    private void setupConnection() {
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            StringBuilder requestBuilder = new StringBuilder();
            String requestLine;
            while((requestLine = reader.readLine()) != null && !requestLine.trim().isEmpty()) {
                requestBuilder.append(requestLine + "\n");
            }
            String httpRequest = requestBuilder.toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            requestHandler.handleRequest(httpRequestProcessor.getMethod(), httpRequestProcessor.getPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
