package ChatApp.server.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer {

    private static final int TCP_PORT = 3000;
    private static final int HTTP_PORT = 8081;
    private static final int NUM_THREADS = 50;

    public void start() {
        new Thread( () -> { setupTCP(); }).start();
        new Thread( () -> { setupHTTP(); }).start();
    }

    private void setupTCP() {
        ExecutorService pool = Executors.newFixedThreadPool(NUM_THREADS);
        try (ServerSocket server = new ServerSocket(TCP_PORT)) {
            while(true) {
                try {
                    Socket socket = server.accept();
                    setupNewConnection(pool, socket);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void setupHTTP() {
        ExecutorService pool = Executors.newFixedThreadPool(NUM_THREADS);
        try (ServerSocket server = new ServerSocket(HTTP_PORT)) {
            while(true) {
                try {
                    Socket socket = server.accept();
                    InnerHttpConnection connection = new InnerHttpConnection(socket);
                    pool.submit(connection);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void setupNewConnection(ExecutorService pool, Socket socket) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String initMessage = reader.readLine();
            String username = initMessage.substring(initMessage.indexOf("|")+1, initMessage.lastIndexOf("|"));
            String roomID = initMessage.substring(initMessage.lastIndexOf("|")+1);
            Room room = ChatServerDataStorage.getRoomById(Integer.parseInt(roomID));
            TcpConnection connection = new TcpConnection(socket, room, username);
            room.addConnection(connection);
            pool.submit(connection);
            room.sendChatHistory(connection);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ChatServer server = new ChatServer();
        server.start();
    }
}
