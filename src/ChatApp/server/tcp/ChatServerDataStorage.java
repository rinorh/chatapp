package ChatApp.server.tcp;

import java.util.*;

public class ChatServerDataStorage {
    private static Map<Integer, Room> rooms = new HashMap<>();
    private static List<String> tokens = new ArrayList<>();

    public static void saveToken(String token) {
        tokens.add(token);
    }

    public static List<String> getAllTokens() {
        return tokens;
    }

    public static void createNewRoom(String newRoomName) {
        Room room = new Room(newRoomName);
        rooms.put(room.getID(), room);
    }

    public static Room getRoomById(int roomId) {
        return rooms.get(roomId);
    }

    public static void leaveRoom(String roomIDStr, String username) {
        int roomID = Integer.parseInt(roomIDStr);
        rooms.get(roomID).removeClient(username);
    }

    public static void exitUser(String token) {
        for(int i = 0; i < tokens.size(); i++) {
            if(tokens.get(i).equals(token)) {
                tokens.remove(i);
                break;
            }
        }
    }

    public static List<Room> getRooms() {
        Collection<Room> roomsSet = rooms.values();
        List<Room> roomsList = new ArrayList<>();
        for(Room room : roomsSet) {
            roomsList.add(room);
        }
        return roomsList;
    }
}
