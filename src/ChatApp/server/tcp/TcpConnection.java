package ChatApp.server.tcp;

import java.io.*;
import java.net.Socket;

public class TcpConnection implements Runnable {

    private String username;
    private Socket socket;
    private Room room;
    private BufferedWriter writer;
    private BufferedReader reader;

    public TcpConnection(Socket socket, Room room, String username) {
        this.socket = socket;
        this.room = room;
        this.username = username;
        setupConnection();
    }

    private void setupConnection() {
        try {
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (!socket.isClosed()) {
                String message = reader.readLine();
                if(message == null || message.equals("LEAVE")) {
                    socket.close();
                } else {
                    room.sendToAll(this, message);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public void sendMessage(String message) {
        try {
            writer.write(message + "\n");
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
