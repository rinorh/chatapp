package ChatApp.server.tcp;

import ChatApp.httpUtils.HttpResponse;
import ChatApp.httpUtils.HttpResponseProcessor;
import ChatApp.httpUtils.RequestHandler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Date;
import java.util.List;

public class InnerHttpRequestHandler implements RequestHandler {

    private static final int REMOTE_HTTP_PORT = 8080;

    private Socket socket;

    public InnerHttpRequestHandler(Socket socket) {
        this.socket = socket;
    }

    public void handleRequest(String method, String path) {
        if(method.equals("GET")) {
            handleGetRequest(path);
        } else if(method.equals("POST")) {
            handlePostRequest(path);
        } else if(method.equals("PUT")) {
            handlePutRequest(path);
        } else if(method.equals("DELETE")) {
            handleDeleteRequest(path);
        }
    }

    @Override
    public void handleGetRequest(String path) {
        if(path.equals("/rooms")) {
            getRooms();
        }
    }

    @Override
    public void handlePostRequest(String path) {
        if(path.startsWith("/register/")) {
            register(path);
        } else if (path.startsWith("/room/")) {
            tryToEnterRoom(path);
        } else if (path.startsWith("/newroom/")) {
            createNewRoom(path);
        } else if(path.startsWith("/leave/")) {
            leaveRoom(path);
        } else if(path.startsWith("/exit/")) {
            removeUser(path);
        }
    }

    @Override
    public void handlePutRequest(String path) {
        //handle put requests
    }

    @Override
    public void handleDeleteRequest(String path) {
        //handle delete requests
    }

    private void getRooms() {
        List<Room> roomsAvailable = ChatServerDataStorage.getRooms();
        StringBuilder responseContentBuilder = new StringBuilder();
        for(Room room : roomsAvailable) {
            responseContentBuilder.append(room.getName()+"|"+room.getID()+";");
        }
        String httpResponse = new HttpResponse().code("HTTP/1.1 200 OK").date(new Date().toString()).server("TCP Server")
                .lastModified(new Date().toString()).transferEncoding("chunked").connection("keep-alive").contentType("text/html; charset=UTF-8")
                .content(responseContentBuilder.toString()).toString();
        try {
            System.out.println("Writing: " + httpResponse);
            BufferedWriter curWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            curWriter.write(httpResponse + "\n");
            curWriter.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void register(String path) {
        String token = path.substring(path.lastIndexOf("/")+1);
        ChatServerDataStorage.saveToken(token);
        System.out.println("Token added " + token);
        String httpResponse = new HttpResponse().code("HTTP/1.1 200 OK").date(new Date().toString()).server("TCP Server")
                .lastModified(new Date().toString()).transferEncoding("chunked").connection("keep-alive").contentType("text/html; charset=UTF-8")
                .content("").toString();
        try (Socket socket = new Socket("localhost", REMOTE_HTTP_PORT)) {
            System.out.println("Writing: " + httpResponse);
            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor(httpResponse);
            httpResponseProcessor.sendResponse(socket);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void tryToEnterRoom(String path) {
        String subPath = path.substring(1);
        String room = subPath.substring(subPath.indexOf("/")+1, subPath.lastIndexOf("/"));
        String token = subPath.substring(subPath.lastIndexOf("/")+1);
        String responseCode = getResponseCodeByTokenAuthentication(token);
        sendRoomResponseToClient(room, responseCode);
    }

    private String getResponseCodeByTokenAuthentication(String token) {
        String responseCode;
        if(ChatServerDataStorage.getAllTokens().contains(token)) {
            responseCode = "200 OK";
        } else {
            responseCode = "400 BAD_REQUEST";
        }
        return responseCode;
    }

    private void sendRoomResponseToClient(String room, String responseCode) {
        String httpResponse = new HttpResponse().code("HTTP/1.1 " + responseCode).date(new Date().toString()).server("TCP Server")
                .lastModified(new Date().toString()).transferEncoding("chunked").connection("keep-alive").contentType("text/html; charset=UTF-8")
                .content(room).toString();
        try {
            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor(httpResponse);
            httpResponseProcessor.sendResponse(socket);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void leaveRoom(String path) {
        String subPath = path.substring(1);
        String room = subPath.substring(subPath.indexOf("/") + 1, subPath.lastIndexOf("/"));
        String username = subPath.substring(subPath.lastIndexOf("/") + 1);
        ChatServerDataStorage.leaveRoom(room, username);
    }

    private void createNewRoom(String path) {
        String newRoomName = path.substring(path.lastIndexOf("/")+1);
        ChatServerDataStorage.createNewRoom(newRoomName);
    }

    private void removeUser(String path) {
        String token = path.substring(path.lastIndexOf("/")+1);
        ChatServerDataStorage.exitUser(token);
    }
}
