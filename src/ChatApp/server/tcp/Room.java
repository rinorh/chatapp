package ChatApp.server.tcp;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private static int lastID = 0;

    private int ID;
    private String name;
    private List<String> messages = new ArrayList<>();
    private List<TcpConnection> connections = new ArrayList<>();

    public Room(String name) {
        this.name = name;
        ID = lastID++;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void addConnection(TcpConnection connection) {
        connections.add(connection);
        sendToAllExceptMe(connection, connection.getUsername() + " joined the room!");
    }

    public void removeClient(String username) {
        int userindex = -1;
        for(int i = 0; i < connections.size(); i++) {
            if(connections.get(i).getUsername().equals(username)) {
                userindex = i;
                break;
            }
        }
        connections.remove(userindex);
        sendToAll(username + " left the chatroom!");
    }

    public void sendToAll(TcpConnection connection, String message) {
        String fullMessage = connection.getUsername() + ": " + message;
        messages.add(fullMessage);
        for(TcpConnection conn : connections) {
            if(conn != connection) {
                conn.sendMessage(fullMessage);
            }
        }
    }

    public void sendToAllExceptMe(TcpConnection connection, String message) {
        messages.add(message);
        for(TcpConnection conn : connections) {
            if(conn != connection) {
                conn.sendMessage(message);
            }
        }
    }

    public void sendToAll(String message) {
        if (message.equals("LEAVE")) return;
        messages.add(message);
        for(TcpConnection conn : connections) {
            conn.sendMessage(message);
        }
    }

    public void sendChatHistory(TcpConnection connection) {
        StringBuilder chatHistory = new StringBuilder();
        for(String message : messages) {
            chatHistory.append(message + "\n");
        }
        connection.sendMessage(chatHistory.toString());
    }

    @Override
    public String toString() {
        return name;
    }
}
