package ChatApp.server.http;

import ChatApp.httpUtils.HttpRequestProcessor;
import ChatApp.httpUtils.HttpResponseProcessor;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class HttpConnection implements Runnable {

    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private HttpRequestHandler requestHandler;

    public HttpConnection(Socket socket) {
        this.socket = socket;
        this.requestHandler = new HttpRequestHandler(socket);
        setupConnection();
    }

    private void setupConnection() {
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            StringBuilder dataBuilder = new StringBuilder();
            String dataLine;
            while((dataLine = reader.readLine()) != null && !dataLine.trim().isEmpty()) {
                dataBuilder.append(dataLine + "\n");
            }
            String httpData = dataBuilder.toString();
            boolean isResponse = httpData.startsWith("HTTP");
            if(isResponse) {
                HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor(httpData);
                handleResponse(httpResponseProcessor);
            } else {
                HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpData);
                requestHandler.handleRequest(httpRequestProcessor.getMethod(), httpRequestProcessor.getPath());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void handleResponse(HttpResponseProcessor httpResponseProcessor) {
        String[] rooms = httpResponseProcessor.getContent().split(";");
        System.out.println(Arrays.toString(rooms));
    }
}
