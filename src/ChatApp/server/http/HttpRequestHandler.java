package ChatApp.server.http;

import ChatApp.httpUtils.*;
import jdk.nashorn.internal.objects.annotations.Getter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Date;

public class HttpRequestHandler implements RequestHandler {

    private static final int REMOTE_HTTP_PORT = 8081;

    private Socket socket;

    public HttpRequestHandler(Socket socket) {
        this.socket = socket;
    }

    public void handleRequest(String method, String path) {
        if(method.equals("GET")) {
            System.out.println("Entered handling request on HTTP server...");
            handleGetRequest(path);
        } else if(method.equals("POST")) {
            handlePostRequest(path);
        } else if(method.equals("PUT")) {
            handlePutRequest(path);
        } else {
            System.out.println("Something went wrong!");
        }
    }

    @Override
    public void handleGetRequest(String path) {
        if(path.equals("/rooms")) {
            sendAvailableRooms(socket);
        } else if (path.startsWith("/register")) {
            String username = path.substring(path.lastIndexOf("/")+1);
            registerUser(socket, username);
        } else if (path.startsWith("/room/")) {
            String subPath = path.substring(1);
            String room = subPath.substring(subPath.indexOf("/")+1, subPath.lastIndexOf("/"));
            String token = subPath.substring(subPath.lastIndexOf("/")+1);
            enterRoom(socket, room, token);
        }
    }

    @Override
    public void handlePostRequest(String path) {
        if(path.startsWith("/newroom/")) {
            String newRoomName = path.substring(path.lastIndexOf("/") + 1);
            createNewRoom(newRoomName);
        } else if(path.startsWith("/leave/")) {
            String subPath = path.substring(1);
            String room = subPath.substring(subPath.indexOf("/") + 1, subPath.lastIndexOf("/"));
            String username = subPath.substring(subPath.lastIndexOf("/") + 1);
            leaveRoom(room, username);
        } else if (path.startsWith("/exit/")) {
            userExit(path);
        }
    }

    @Override
    public void handlePutRequest(String path) {
        //put requests
    }

    @Override
    public void handleDeleteRequest(String path) {
        //delete requests
    }

    public void sendAvailableRooms(Socket socket) {
        String rooms = getRoomsFromTcpServer();
        sendAvailableRoomsToClient(rooms, socket);
    }

    private String getRoomsFromTcpServer() {
        String rooms = null;
        try (Socket tcpSocket = new Socket("localhost", REMOTE_HTTP_PORT)){
            String httpRequest = new HttpRequest().method("GET").path("/rooms").version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(tcpSocket);

            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor();
            httpResponseProcessor.getResponse(tcpSocket);
            rooms = httpResponseProcessor.getContent();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return rooms;
    }

    private void sendAvailableRoomsToClient(String rooms, Socket socket) {
        try {
            String httpResponse = new HttpResponse().code("HTTP/1.1 200 OK").date(new Date().toString()).server("TCP Server")
                    .lastModified(new Date().toString()).transferEncoding("chunked").connection("keep-alive").contentType("text/html; charset=UTF-8")
                    .content(rooms).toString();
            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor(httpResponse);
            httpResponseProcessor.sendResponse(socket);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void registerUser(Socket socket, String username) {
        String token = generateToken(username);
        respondSuccessfullyToClient(token, socket);
        saveUserInTcpServer(token);
    }

    private void respondSuccessfullyToClient(String token, Socket socket) {
        try {
            String httpResponse = new HttpResponse().code("HTTP/1.1 200 OK").date(new Date().toString()).server("TCP Server")
                    .lastModified(new Date().toString()).transferEncoding("chunked").connection("keep-alive").contentType("text/html; charset=UTF-8")
                    .content(token).toString();
            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor(httpResponse);
            httpResponseProcessor.sendResponse(socket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void saveUserInTcpServer(String token) {
        try (Socket tcpSocket = new Socket("localhost", REMOTE_HTTP_PORT)){
            HttpRequest httpRequest = new HttpRequest().method("POST").path("/register/" + token).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048");
            BufferedWriter tcpWriter = new BufferedWriter(new OutputStreamWriter(tcpSocket.getOutputStream()));
            tcpWriter.write(httpRequest.toString());
            tcpWriter.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void enterRoom(Socket socket, String room, String token) {
        boolean canEnter = canEnterRoom(room, token);
        canEnterResponseToClient(canEnter, room, socket);
    }

    private boolean canEnterRoom(String room, String token) {
        boolean canEnter = false;
        try (Socket tcpSocket = new Socket("localhost", REMOTE_HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path("/room/" + room + "/" + token).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(tcpSocket);

            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor();
            httpResponseProcessor.getResponse(tcpSocket);
            canEnter =  httpResponseProcessor.getCode().equals("200");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return canEnter;
    }

    private void canEnterResponseToClient(boolean canEnter, String room, Socket socket) {
        if(canEnter) {
            String httpResponse = new HttpResponse().code("HTTP/1.1 200 OK").date(new Date().toString()).server("HTTP Server")
                    .lastModified(new Date().toString()).transferEncoding("chunked").connection("keep-alive").contentType("text/html; charset=UTF-8")
                    .content(room).toString();
            try {
                HttpResponseProcessor httpResponseProcessor1 = new HttpResponseProcessor(httpResponse);
                httpResponseProcessor1.sendResponse(socket);
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void leaveRoom(String room, String username) {
        try (Socket tcpSocket = new Socket("localhost", REMOTE_HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path("/leave/" + room + "/" + username).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(tcpSocket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void userExit(String path) {
        try (Socket tcpSocket = new Socket("localhost", REMOTE_HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path(path).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(tcpSocket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void createNewRoom(String newRoomName) {
        try (Socket tcpSocket = new Socket("localhost", REMOTE_HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path("/newroom/" + newRoomName).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(tcpSocket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String generateToken(String username) {
        return Integer.toString(username.hashCode());
    }
}
