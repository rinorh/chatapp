package ChatApp.server.http;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HTTPServer {

    private static final int THIS_HTTP_PORT = 8080;
    private static final int NUM_THREADS = 50;

    public void start() {
        setupHTTPServer();
    }

    private void setupHTTPServer() {
        ExecutorService pool = Executors.newFixedThreadPool(NUM_THREADS);
        try (ServerSocket server = new ServerSocket(THIS_HTTP_PORT)) {
            while(true) {
                try {
                    Socket socket = server.accept();
                    HttpConnection connection = new HttpConnection(socket);
                    System.out.println("New user connected..." + socket);
                    pool.submit(connection);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        HTTPServer server = new HTTPServer();
        server.start();
    }
}
