package ChatApp.client;

import ChatApp.httpUtils.HttpRequest;
import ChatApp.httpUtils.HttpRequestProcessor;
import ChatApp.httpUtils.HttpResponseProcessor;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ChatClient {

    private static final int HTTP_PORT = 8080;
    private static final int TCP_PORT = 3000;
    private static final String HOST = "localhost";

    private Socket socket;
    private String username;
    private BufferedWriter writer;
    private BufferedReader reader;
    private String token;
    private String room;
    private Scanner scanner = new Scanner(System.in);

    public void register() {
        System.out.print("Enter a username: ");
        username = scanner.nextLine();
        try (Socket socket = new Socket(HOST, HTTP_PORT)) {
             String httpRequest = new HttpRequest().method("GET").path("/register/" + username).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
             HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
             httpRequestProcessor.sendRequest(socket);

             HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor();
             httpResponseProcessor.getResponse(socket);
             token = httpResponseProcessor.getContent();
             System.out.println("You have been registered!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void startUsingApp() {
        takeAction();
        if(room == null) return;
        enterSelectedRoom();
    }

    private void enterSelectedRoom() {
        establishConnection();
        sendInitMessage();
        start();
    }

    private void establishConnection() {
        try {
            socket = new Socket(HOST, TCP_PORT);
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sendInitMessage() {
        try {
            writer.write("INIT|" + username + "|" + room + "\n");
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void start() {
        System.out.println("Now you can start chatting...");
        MessageListener listener = new MessageListener(reader, this);
        Thread listenerThread = new Thread(listener);
        listenerThread.start();
        MessageSender sender = new MessageSender(writer, this, scanner);
        Thread senderThread = new Thread(sender);
        senderThread.start();
    }

    private void takeAction() {
        System.out.println("Press 1 to enter a room.");
        System.out.println("Press 2 to create a new room.");
        System.out.println("Press 3 to exit app.");
        String choice = scanner.nextLine();

        if(Integer.parseInt(choice) == 1) {
            showAllRooms();
            System.out.print("Press the corresponding number: ");
            room = scanner.nextLine();
        } else if(Integer.parseInt(choice) == 2) {
            createNewRoom();
            System.out.print("Press the corresponding number: ");
            room = scanner.nextLine();
        } else {
            exit();
            room = null;
        }
    }

    private void showAllRooms() {
        try (Socket socket = new Socket(HOST, HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("GET").path("/rooms").version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(socket);

            HttpResponseProcessor httpResponseProcessor = new HttpResponseProcessor();
            httpResponseProcessor.getResponse(socket);
            if(httpResponseProcessor.getContent().isEmpty()) {
                System.out.print("There are no rooms created yet, enter a name for a new room: ");
                createNewRoom();
            } else {
                String[] rooms = httpResponseProcessor.getContent().split(";");
                System.out.println("Rooms available:");
                for(int i = 0; i < rooms.length; i++) {
                    String roomName = rooms[i].substring(0, rooms[i].indexOf("|"));
                    String roomID = rooms[i].substring(rooms[i].indexOf("|")+1);
                    System.out.println(roomID + ". " + roomName);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void createNewRoom() {
        System.out.print("Enter new room name: ");
        String newRoomName = scanner.nextLine();

        try (Socket socket = new Socket(HOST, HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path("/newroom/" + newRoomName).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(socket);

            showAllRooms();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void leaveRoom() {
        try (Socket socket = new Socket(HOST, HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path("/leave/" + room + "/" + username).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(socket);
            this.socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        startUsingApp();
    }

    public Socket getSocket(){
        return socket;
    }

    public void exit() {
        try (Socket socket = new Socket(HOST, HTTP_PORT)) {
            String httpRequest = new HttpRequest().method("POST").path("/exit/" + token).version("HTTP/1.1").host("localhost:8080")
                    .userAgent("Mozilla/5.0").accept("text/html").acceptLanguage("en-US").acceptEncoding("gzip, deflate")
                    .connection("keep-alive").upgradeInsecureRequests("1").contentType("text").contentLength("2048").toString();
            HttpRequestProcessor httpRequestProcessor = new HttpRequestProcessor(httpRequest);
            httpRequestProcessor.sendRequest(socket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ChatClient client = new ChatClient();
        client.register();
        client.startUsingApp();
    }

}
