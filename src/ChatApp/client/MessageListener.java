package ChatApp.client;

import java.io.BufferedReader;
import java.io.IOException;

public class MessageListener implements Runnable {

    private BufferedReader reader;
    private ChatClient client;

    public MessageListener(BufferedReader reader, ChatClient client) {
        this.reader = reader;
        this.client = client;
    }

    @Override
    public void run() {
        try {
            String remoteMessage;
            while(true) {
                remoteMessage = reader.readLine();
                if(client.getSocket().isClosed()) {
                    break;
                }
                if(remoteMessage != null)   System.out.println(remoteMessage);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
