package ChatApp.client;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

public class MessageSender implements Runnable {

    private static final String LEAVE_KEYWORD = "LEAVE";

    private BufferedWriter writer;
    private ChatClient client;
    private Scanner scanner;

    public MessageSender(BufferedWriter writer, ChatClient client, Scanner scanner) {
        this.writer = writer;
        this.client = client;
        this.scanner = scanner;
    }

    @Override
    public void run() {
        try {
            String message;
            do {
                message = scanner.nextLine();
                writer.write(message + "\n");
                writer.flush();
            } while (!message.equals(LEAVE_KEYWORD));
            client.leaveRoom();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
