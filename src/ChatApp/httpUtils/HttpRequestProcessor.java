package ChatApp.httpUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class HttpRequestProcessor {
    private String request;
    private String[] tokens;

    public HttpRequestProcessor(String request) {
        this.request = request;
        this.tokens = request.split("\n");
    }

    public String getMethod() {
        final String method = tokens[0].substring(0, tokens[0].indexOf(" "));
        return method;
    }

    public String getPath() {
        String path = tokens[0].substring(tokens[0].indexOf(" ") + 1);
        path = path.substring(0, path.indexOf(" "));
        return path;
    }

    public void sendRequest(Socket socket) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        writer.write(request + "\n");
        writer.flush();
    }
}
