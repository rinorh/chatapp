package ChatApp.httpUtils;

public interface RequestHandler {

    void handleGetRequest(String path);

    void handlePostRequest(String path);

    void handlePutRequest(String path);

    void handleDeleteRequest(String path);

}
