package ChatApp.httpUtils;

public class HttpResponse {
    private String code = "";
    private String date = "";
    private String server = "";
    private String lastModified = "";
    private String transferEncoding = "";
    private String connection = "";
    private String contentType = "";
    private String content = "";

    public HttpResponse code(String code) {
        this.code = code;
        return this;
    }

    public HttpResponse date(String date) {
        this.date = date;
        return this;
    }

    public HttpResponse server(String server) {
        this.server = server;
        return this;
    }

    public HttpResponse lastModified(String lastModified) {
        this.lastModified = lastModified;
        return this;
    }

    public HttpResponse transferEncoding(String transferEncoding) {
        this.transferEncoding = transferEncoding;
        return this;
    }

    public HttpResponse connection(String connection) {
        this.connection = connection;
        return this;
    }

    public HttpResponse contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public HttpResponse content(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(code + "\n").append("date: " + date + "\n").append("server: " + server + "\n").append("last-modified: " + lastModified + "\n")
                .append("transfer-encoding: " + transferEncoding + "\n").append("connection: " + connection + "\n").append("content-type: " + contentType + "\n")
                .append("content: " + content + "\n").toString();
    }
}
