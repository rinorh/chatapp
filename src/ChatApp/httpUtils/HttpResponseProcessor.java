package ChatApp.httpUtils;

import java.io.*;
import java.net.Socket;

public class HttpResponseProcessor {
    private String response;
    private String[] tokens;

    public HttpResponseProcessor() {

    }

    public HttpResponseProcessor(String response) {
        this.response = response;
        tokens = response.split("\n");
    }

    public String getCode() {
        if(tokens == null) {
            return null;
        }
        return tokens[0].substring(tokens[0].indexOf(" ")+1, tokens[0].lastIndexOf(" "));
    }

    public String getContent()  {
        if(tokens == null) {
            return null;
        } else if (tokens.length < 8) {
            return null;
        }
        return tokens[7].substring(tokens[7].indexOf("content: ") + 9);
    }

    public String getResponse(Socket socket) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            builder.append(line + "\n");
        }
        this.response = builder.toString();
        tokens = response.split("\n");
        return response;
    }

    public void sendResponse(Socket socket) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        writer.write(response + "\n");
        writer.flush();
    }
}
