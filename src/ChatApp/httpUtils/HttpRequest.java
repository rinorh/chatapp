package ChatApp.httpUtils;

public class HttpRequest {

    private String method = "";
    private String path = "";
    private String version = "";
    private String host = "";
    private String userAgent = "";
    private String accept = "";
    private String acceptLanguage = "";
    private String acceptEncoding = "";
    private String connection = "";
    private String upgradeInsecureRequests = "";
    private String contentType = "";
    private String contentLength = "";

    public HttpRequest method(String method) {
        this.method = method;
        return this;
    }

    public HttpRequest path(String path) {
        this.path = path;
        return this;
    }

    public HttpRequest version(String version) {
        this.version = version;
        return this;
    }

    public HttpRequest host(String host) {
        this.host = host;
        return this;
    }

    public HttpRequest userAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public HttpRequest accept(String accept) {
        this.accept = accept;
        return this;
    }

    public HttpRequest acceptLanguage(String acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
        return this;
    }

    public HttpRequest acceptEncoding(String acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
        return this;
    }
    public HttpRequest connection(String connection) {
        this.connection = connection;
        return this;
    }

    public HttpRequest upgradeInsecureRequests(String upgradeInsecureRequests) {
        this.upgradeInsecureRequests = upgradeInsecureRequests;
        return this;
    }

    public HttpRequest contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public HttpRequest contentLength(String contentLength) {
        this.contentLength = contentLength;
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(method+" ").append(path+" ").append(version+"\n").append("host: " +host+"\n").append("user-agent: " +userAgent+"\n")
                     .append("accept: " +accept+"\n").append("accept-language: " + acceptLanguage+"\n").append("accept-encoding: " + acceptEncoding+"\n").append("connection: "+connection+"\n")
                     .append("upgrade-insecure-requests: " + upgradeInsecureRequests+"\n").append("content-type: " + contentType+"\n").append("content-length: " + contentLength+"\n").toString();
    }
}
